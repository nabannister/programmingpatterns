using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandQueue {
    private List<Command> queue = new List<Command> ();
    private int index = 0;
    private bool ready = true;

    public void QueueCommand (Command command) {
        if (command.HasValidExecute()) {
            queue.RemoveRange (index, queue.Count - index);
            queue.Add (command);
        }
    }

    public IEnumerator Execute () {
        if (CanExecute ()) {
            ready = false;
            yield return (queue [index].Execute ());
            ready = true;
            index++;
        }
    }

    public IEnumerator Undo () {
        if (CanUndo ()) {
            ready = false;
            yield return (queue [index - 1].Undo ());
            ready = true;
            index--;
        }
    }

    public bool CanExecute () {
        return queue.Count > 0 && index < queue.Count && ready;
    }

    public bool CanUndo () {
        return queue.Count > 0 && index > 0 && ready &&
            queue [index - 1].HasValidUndo();
    }

    public bool IsReady () {
        return ready;
    }
}
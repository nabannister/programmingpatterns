using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Command {
    public Command () { }
    public Command (Coroutine execute, Coroutine undo) {
        ExecuteCoroutine = execute;
        UndoCoroutine = undo;
    }
    public Command (Delegate execute, Delegate undo) {
        ExecuteDelegate = execute;
        UndoDelegate = undo;
        ExecuteCoroutine = ExecuteWrapper;
        UndoCoroutine = UndoWrapper;
    }

    public delegate void Delegate ();
    public delegate IEnumerator Coroutine ();

    protected Coroutine ExecuteCoroutine;
    protected Coroutine UndoCoroutine;
    
    protected Delegate ExecuteDelegate;
    protected Delegate UndoDelegate;

    public bool HasValidExecute () {
        return ExecuteCoroutine != null;
    }

    public bool HasValidUndo () {
        return UndoCoroutine != null;
    }

    public virtual IEnumerator Execute () {
        if (HasValidExecute ()) {
            yield return (ExecuteCoroutine.Invoke());
        }
    }

    public virtual IEnumerator Undo () {
        if (HasValidUndo ()) {
            yield return (UndoCoroutine.Invoke ());
        }
    }

    protected virtual IEnumerator ExecuteWrapper () {
        ExecuteDelegate.Invoke ();
        return null;
    }

    protected virtual IEnumerator UndoWrapper () {
        UndoDelegate.Invoke ();
        return null;
    }
}
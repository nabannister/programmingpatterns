using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommandExample : MonoBehaviour {

    public Image image;
    public Button redoButton, undoButton;
    public Color[] Colors;
    private CommandQueue queue = new CommandQueue ();

    public IEnumerator SetColor (Color color) {
        if (queue.IsReady ()) {
            Color oldColor = image.color;
            Command ColorCommand = new Command
                (() => LerpToColor (color, oldColor), 
                 () => LerpToColor (oldColor, color));

            queue.QueueCommand (ColorCommand);
            yield return StartCoroutine (queue.Execute ());
            UpdateButtons ();
        }
    }

    public void SetColor (int colorID) {
        StartCoroutine (SetColor (Colors[colorID]));
    }

    private IEnumerator LerpToColor (Color color, Color oldColor) {
        float speed = 10f;
        float t = 0f;
        while (image.color != color) {
            t += 0.007f * speed;
            image.color = Color.Lerp (oldColor, color, t);
            yield return new WaitForSeconds (0.007f);
        }
    }

    public void UndoButton () {
        StartCoroutine (UndoColor());
    }

    private IEnumerator UndoColor () {
        yield return StartCoroutine (queue.Undo ());
        UpdateButtons ();
    }

    public void RedoButton () {
        StartCoroutine (RedoColor ());
    }

    private IEnumerator RedoColor () {
        yield return StartCoroutine (queue.Execute ());
        UpdateButtons ();
    }

    private void UpdateButtons () {
        if (redoButton)
            redoButton.interactable = queue.CanExecute ();
        if (undoButton)
            undoButton.interactable = queue.CanUndo ();
    }

    private void Start () {
        UpdateButtons ();
    }
}